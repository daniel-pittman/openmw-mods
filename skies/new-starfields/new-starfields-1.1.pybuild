# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from pybuild import Pybuild1, InstallDir
from pybuild.modinfo import M
from pyclass import NexusMod


class Mod(NexusMod, Pybuild1):
    NAME = "New Starfields"
    DESC = "A simple, high-definition replacer for the clear night sky texture"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43246"
    LICENSE = "all-rights-reserved"
    RDEPEND = "skies/swg-skies[night-sky-mesh]"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        opacity-100? ( https://download.fliggerty.com/file.php?id=783 -> {M}.rar )
        !opacity-100? ( New_Starfields_-_Alternate_Opacities-43246-1-0.7z )
    """
    RESTRICT = "!opacity-100? ( fetch )"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43246"
    TEXTURE_SIZES = """
        !variant-5? ( 4096 )
        variant-5? ( 8192 )
    """
    IUSE = """
        +variant-0
        variant-1
        variant-2
        variant-3
        variant-4
        variant-5
        variant-6
        variant-7
        opacity-25
        opacity-33
        opacity-50
        +opacity-100
    """
    REQUIRED_USE = """
        ^^ (
            variant-0
            variant-1
            variant-2
            variant-3
            variant-4
            variant-5
            variant-6
            variant-7
        )
        ^^ (
            opacity-25
            opacity-33
            opacity-50
            opacity-100
        )
    """
    INSTALL_DIRS = [
        InstallDir(".", S=f"{M}", RENAME="textures", WHITELIST=["tx_stars"])
    ]

    def src_prepare(self):
        for flag in self.USE:
            # Note that exactly one flag will match due to REQUIRED_USE
            if flag.startswith("opacity-"):
                opacity = flag.lstrip("opacity-")

        for flag in self.USE:
            # Note that exactly one flag will match due to REQUIRED_USE
            if flag.startswith("variant-"):
                variant = "0" + flag.lstrip("variant-")

        dest = os.path.join(f"{M}", "tx_stars.dds")
        if "opacity-100" in self.USE:
            source = f"{M}"
            os.rename(os.path.join(source, f"tx_stars_{variant}.dds"), dest)
        else:
            source = "New_Starfields_-_Alternate_Opacities-43246-1-0"
            os.rename(os.path.join(source, f"tx_stars_{variant}_{opacity}.dds"), dest)
