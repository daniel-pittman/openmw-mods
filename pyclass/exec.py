# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from pybuild import Pybuild1, patch_dir


class Exec(Pybuild1):
    """
    Pybuild class for executables
    """

    PROPERTIES = "exec"

    def src_install(self):
        super().src_install()
        patch_dir(os.path.join(self.WORKDIR, self.S), self.D)
