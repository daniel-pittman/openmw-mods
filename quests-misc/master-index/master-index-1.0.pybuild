# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from pybuild import Pybuild1, InstallDir, File, apply_patch


class Mod(Pybuild1):
    NAME = "Master Index"
    DESC = "Adds a new quest to find all ten Propylon Indices"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind[tribunal]"
    DEPEND = "sys-bin/convert"
    CONFLICTS = ""
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/masterindex.zip
        https://gitlab.com/bmwinger/umopp/uploads/f5319ea22e3a519b28f4d81928affbf8/master_index-umopp-3.0.2.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("master_index.esp")], S="masterindex")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "master_index-umopp-3.0.2")
        apply_patch(os.path.join(path, "master_index_plugin.patch"))
        os.mkdir("Icons/M")
        self.execute(
            "magick convert Icons/Misc_master_index.tga Icons/M/Misc_master_index.dds"
        )
        os.remove("Icons/Misc_master_index.tga")
        apply_patch(os.path.join(path, "master_index_mesh.patch"))
