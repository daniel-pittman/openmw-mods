# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import InstallDir, Pybuild1
from pyclass import NexusMod
from pyclass.maps import OptMaps


class Mod(OptMaps, NexusMod, Pybuild1):
    NAME = "Telvanni - Arkitektora of Vvardenfell"
    DESC = "Vanilla gamma ans style HQ Telvanni and mushrooms retexture."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43530"
    NEXUS_URL = HOMEPAGE
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = "1024 2048"
    SRC_URI = """
        texture_size_1024? (
            AoVv_Telvanni_MQ-43530-1-1.7z
            map_normal? ( AoVv_Telvanni_Bump_Maps_MQ-43530-.7z )
        )
        texture_size_2048? (
            AoVv_Telvanni_HQ-43530-1-1.7z
            map_normal? ( AoVv_Telvanni_Bump_Maps_HQ-43530-1-0.7z )
        )
    """
    IUSE = "map_normal"
    INSTALL_DIRS = [
        InstallDir(
            "MQ/Data Files",
            S="AoVv_Telvanni_MQ-43530-1-1",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "HQ/Data Files",
            S="AoVv_Telvanni_HQ-43530-1-1",
            REQUIRED_USE="texture_size_2048",
        ),
        InstallDir(
            "Bump MQ/Data Files",
            S="AoVv_Telvanni_Bump_Maps_MQ-43530-",
            REQUIRED_USE="map_normal texture_size_1024",
        ),
        InstallDir(
            "Bump HQ/Data Files",
            S="AoVv_Telvanni_Bump_Maps_HQ-43530-1-0",
            REQUIRED_USE="map_normal texture_size_2048",
        ),
    ]
