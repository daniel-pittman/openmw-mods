# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import InstallDir, Pybuild1
from pyclass import NexusMod


class Mod(NexusMod, Pybuild1):
    NAME = "Shacks Docks and Ships - Arkitektora of Vvardenfell"
    DESC = "Contains new textures for wooden shacks, docks and ships"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43520"
    NEXUS_URL = HOMEPAGE
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = "1024 2048"
    SRC_URI = """
        texture_size_1024? ( MQ_Textures-43520-1-0.7z )
        texture_size_2048? ( HQ_Textures-43520-1-0.7z )
    """
    # modding-openmw.com recommends removing these files to avoid overriding other
    # mods that provide wood textures
    BLACKLIST = [
        "Textures/Tx_wood_brown_shelf_corner.dds",
        "Textures/Tx_wood_brown_shelf_corner_01.dds",
        "Textures/Tx_wood_brown_shelf.dds",
    ]
    INSTALL_DIRS = [
        InstallDir("MQ/Data Files", S="MQ_Textures-43520-1-0", BLACKLIST=BLACKLIST),
        InstallDir("HQ/Data Files", S="HQ_Textures-43520-1-0", BLACKLIST=BLACKLIST),
        # textures for Windows glow
        # InstallDir("MQ/Extras/Windows Glow", S="MQ_Textures-43520-1-0"),
        # InstallDir("HQ/Extras/Windows Glow", S="HQ_Textures-43520-1-0"),
    ]
